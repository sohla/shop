import bcrypt from 'bcryptjs'

const data = {
    users: [
        {
          name: 'Sohla',
          email: 'sohla@gmail.com',
          password: bcrypt.hashSync('1234', 8),
          isAdmin: true,
        },
        {
          name: 'Pera',
          email: 'pera@gmail.com',
          password: bcrypt.hashSync('1234', 8),
          isAdmin: false,
        },
      ],
    products: [
        {
            
            name: 'adidas slim shirt',
            category: 'shirts',
            image: '/images/p1.jpg',
            price: 120,
            countInStock: 10,
            brand: 'adidas',
            rating: 4.5,
            numReviews: 10,
            description: 'high quality pridutct'
        },
        {
            
            name: 'puma slim shirt',
            category: 'shirts',
            image: '/images/p2.jpg',
            price: 100,
            countInStock: 20,
            brand: 'puma',
            rating: 4.0,
            numReviews: 10,
            description: 'high quality pridutct'
        },
        {
            
            name: 'lacoste slim shirt',
            category: 'shirts',
            image: '/images/p3.jpg',
            price: 220,
            countInStock: 0,
            brand: 'lacoste',
            rating: 4.8,
            numReviews: 17,
            description: 'high quality pridutct'
        },
        {
            
            name: 'nike slim pante',
            category: 'pants',
            image: '/images/p4.jpg',
            price: 160,
            countInStock: 15,
            brand: 'nike',
            rating: 4.7,
            numReviews: 13,
            description: 'high quality pridutct'
        },
        {
            
            name: 'puma slim shirt',
            category: 'pants',
            image: '/images/p5.jpg',
            price: 190,
            countInStock: 5,
            brand: 'puma',
            rating: 4.5,
            numReviews: 11,
            description: 'high quality pridutct'
        }
    ]
}

export default data;