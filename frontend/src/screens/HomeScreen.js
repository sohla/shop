import React, { useEffect } from 'react';
import Product from '../components/Product';
import MessageBox from '../components/MessageBox';
import LoadingBox from '../components/LoadingBox';
import {useSelector, useDispatch} from 'react-redux'
import { listProducts } from '../actions/productActions';

export default function HomeScreen() {
//    const [products, setProducts] = useState([]);
//    const [loading, s> tLoading] = useState(false);
//    const [error, setError] = useState(false);
    const dispatch = useDispatch()
    const productList  = useSelector((state) => state.productList)
    const { loading, error, products} = productList

    useEffect(() => {
      dispatch(listProducts())
    }, [dispatch]);

    return (
        <div>
            {loading? <LoadingBox></LoadingBox>
            :
            error? <MessageBox varient="danger">{error}</MessageBox>
            :
            <div className="row center">
            {products.map(product =>(
                    <div key={product._id} className="card">
                    <Product key={product._id} product={product}></Product>
                </div>
                ))}
        </div>
            }

        </div>
    )
}
