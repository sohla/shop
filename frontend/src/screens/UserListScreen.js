import React, { useEffect, useState } from 'react';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { useDispatch, useSelector } from 'react-redux';

export default function UserListScreen(props) {

    const userList = useSelector((state) => state.userList);
    const { loading, error, users } = userList;

    const deleteHandler = () => {

    }

    return (
      <div>
      <h1>User list</h1>
      {loading ? (
        <LoadingBox></LoadingBox>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
          <table className="table">
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>EMAIL</th>
                <th>IS SELLER</th>
                <th>IS ADMIN</th>
                <th>ACTIONS</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr key={user._id}>
                  <td>{user._id}</td>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.isSeller ? 'yes' : 'no'}</td>
                  <td>{user.isAdmin ? 'yes' : 'no'}</td>

                  <td>
                    <button
                      type="button"
                      className="small"
                      onClick={() => {
                        props.history.push(`/user/${user._id}/edit`)
                      }}
                    >
                        Edit
                    </button>
                    <button
                        type="button"
                        className="small"
                        onClick={() => deleteHandler(user)}
                    >
                        Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>       
    )
}
