import React from 'react'

export default function ProductEditScreen() {
    const submitHandler = (e) => {

    }
    return (
        <div>
            <form className="form" onSubmit={submitHandler}>
                 <div>
                   <label htmlFor="name">Name</label>
                   <input
                   id="name"
                    type="text"
                    placeholder="Enter name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="image">Image</label>
                   <input
                   id="image"
                    type="text"
                    placeholder="Enter image name"
                    value={image}
                    onChange={(e) => setImage(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="brand">Brand</label>
                   <input
                   id="brand"
                    type="text"
                    placeholder="Enter brand"
                    value={brand}
                    onChange={(e) => setBrand(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="category">category</label>
                   <input
                   id="category"
                    type="text"
                    placeholder="Enter category"
                    value={category}
                    onChange={(e) => setCategory(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="description">description</label>
                   <input
                   id="description"
                    type="text"
                    placeholder="Enter description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="price">price</label>
                   <input
                   id="price"
                    type="text"
                    placeholder="Enter price"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                ></input>
                </div>

                <div>
                   <label htmlFor="name">count in stock</label>
                   <input
                   id="countInStock"
                    type="text"
                    placeholder="Enter count in stock"
                    value={countInStock}
                    onChange={(e) => setCountInStock(e.target.value)}
                ></input>
                </div>

        </form>
        </div>
    )
}
