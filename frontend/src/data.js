import bcrypt from 'bcryptjs'

const data = {

    
    users: [
        {
          name: 'sohla',
          email: 'sohla@gmail.com',
          password: bcrypt.hashSync('1234', 8),
          isAdmin: true,

        },
        {
          name: 'John',
          email: 'user@example.com',
          password: bcrypt.hashSync('1234', 8),
          isAdmin: false,
        },
      ],

      
    products: [
        {
            _id: '1',
            name: 'adidas slim shirt',
            category: 'shirts',
            image: '/images/p1.jpg',
            price: 120,
            countInStock: 10,
            brand: 'adidas',
            rating: 4.5,
            numReviews: 10,
            decription: 'high quality pridutct'
        },
        {
            _id: '2',
            name: 'puma slim shirt',
            category: 'shirts',
            image: '/images/p2.jpg',
            price: 100,
            countInStock: 20,
            brand: 'puma',
            rating: 4.0,
            numReviews: 10,
            decription: 'high quality pridutct'
        },
        {
            _id: '3',
            name: 'lacoste slim shirt',
            category: 'shirts',
            image: '/images/p3.jpg',
            price: 220,
            countInStock: 0,
            brand: 'lacoste',
            rating: 4.8,
            numReviews: 17,
            decription: 'high quality pridutct'
        },
        {
            _id: '4',
            name: 'nike slim pante',
            category: 'pants',
            image: '/images/p4.jpg',
            price: 160,
            countInStock: 15,
            brand: 'nike',
            rating: 4.7,
            numReviews: 13,
            decription: 'high quality pridutct'
        },
        {
            _id: '5',
            name: 'puma slim shirt',
            category: 'pants',
            image: '/images/p5.jpg',
            price: 190,
            countInStock: 5,
            brand: 'puma',
            rating: 4.5,
            numReviews: 11,
            decription: 'high quality pridutct'
        }
    ]
}

export default data;